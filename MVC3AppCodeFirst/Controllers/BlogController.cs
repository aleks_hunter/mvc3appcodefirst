﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC3AppCodeFirst.Models;

namespace MVC3AppCodeFirst.Controllers
{ 
    public class BlogController : Controller
    {
        private BlogContext db = new BlogContext();

        //
        // GET: /Blog/
        public ActionResult About()
        {
            return View();
        }

        public ActionResult Index()
        {
            using (var db = new BlogContext())
            {
                
            return View(db.Blogs.ToList());
            }
            
        }

        //
        // GET: /Blog/Details

        public ViewResult Details(int id)
        {
            Blog blog = db.Blogs.Find(id);
            return View(blog);
        }

        //
        // GET: /Blog/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Blog/Create

        [HttpPost]
        public ActionResult Create(Blog newBlog)
        {
        try        
        {
        using(var db = new BlogContext())
	    {
		     db.Blogs.Add(newBlog);
             db.SaveChanges();
               
        }
             return RedirectToAction("Index");  
        }
        catch
        {   
        return View();
        }
}
        
        //
        // GET: /Blog/Edit
 
        public ActionResult Edit(int id)
        {
            using (var db = new BlogContext())
            {
                return View(db.Blogs.Find(id));
            }
        }

        //
        // POST: /Blog/Edit

        [HttpPost]
        public ActionResult Edit(Blog blog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blog);
        }

        //
        // GET: /Blog/Delete
 
        public ActionResult Delete(int id)
        {
            Blog blog = db.Blogs.Find(id);
            return View(blog);
        }

        //
        // POST: /Blog/Delete

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Blog blog = db.Blogs.Find(id);
            db.Blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}